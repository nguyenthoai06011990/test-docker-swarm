FROM nikolaik/python-nodejs:python2.7-nodejs12 as builder

WORKDIR /app

COPY ./package.json ./
# RUN yarn install --frozen-lockfile
RUN npm rebuild bcrypt --update-binary
# Copy project files into the docker image
RUN npm install 
COPY ./ ./ 

CMD [ "node", "app.js" ]
EXPOSE 5004